import React from 'react';
import ReactDOM from 'react-dom';
import Navbar from './template/navbar.js';
import Loginform from'./body/loginform.js';
import Home from'./body/home.js';
import Testes6 from './body/testes6.js';
import { BrowserRouter as Router, Route,Link} from "react-router-dom";


  class App extends React.Component {
    constructor(props){
      super(props);
      this.state = { apiResponse: ""};
    }
    callAPI(){;
      fetch ('http://localhost:5001/',{mode: 'cors'})
        .then(({ results }) => this.setState({ person: results }));

    }
    componentDidMount(){
      console.log('component did mount');
      this.callAPI();

    }
    render() {
      return (
      <Router>
        <div>

        <Navbar />
        <p>{this.state.apiResponse}</p>
        <Route path="/" exact component={Home} />
        <Route path="/loginform" component={Loginform} />
        <Route path="/testes6" component={Testes6} />
        </div>
      </Router>
      )
    }
  }


ReactDOM.render(
  <App />
  ,
  document.getElementById('root')
);

// ReactDOM.render(
//   <body />,
//   document.getElementById('body')
// );
