import React, { Component } from 'react';
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import './loginform.css'



class loginform extends Component {
  constructor(props) {
  super(props);
  this.state = { username: '',psw:'' };
  this.handleChange = this.handleChange.bind(this);
  this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleChange(event) {
    this.setState({
      [event.target.id]:event.target.value
      });
  }
  handleSubmit = (event) => {
    alert('A user was submitted: ' + this.state.username +' and psw:' + this.state.password);
    event.preventDefault();
  }
  render() {
    return (
      <div className="Login">
      <h2>Login Form</h2>

      <Form onSubmit={this.handleSubmit}>

        <Form.Group controlId="username" bssize="large" >
          <Form.Label>Username</Form.Label>
          <Form.Control type="text" placeholder="Enter Username" value={this.state.username} onChange={this.handleChange}  required/>
        </Form.Group>

        <Form.Group controlId="password" bssize="large">
          <Form.Label>Password</Form.Label>
          <Form.Control type="password" placeholder="Enter Password" value={this.state.password} onChange={this.handleChange}  required/>
        </Form.Group>
        <Button type="submit">Login</Button>
      </Form>
      </div>

)
}
}

export default loginform;
