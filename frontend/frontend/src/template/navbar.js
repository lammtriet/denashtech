import React, { Component } from 'react';
import './navbar.css';
import { BrowserRouter as Router, Route,Link } from "react-router-dom";


class App extends Component{
  render() {
    return(
    <ul>
      <li>
              <Link to="/">Home</Link>
      </li>
      <li>
              <Link to="/loginform">Login</Link>
      </li>
      <li>
              <Link to="/testes6">Test ES6</Link>
      </li>
    </ul>
  )}
}
export default App;
