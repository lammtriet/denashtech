const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors')

// create express app
const app = express();
const dbConfig = require('./config/connectdb.config.js');
const mongoose = require('mongoose');

// Require Notes routes
require('./routes/note.route.js')(app);

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))

// parse requests of content-type - application/json
app.use(bodyParser.json())



mongoose.Promise = global.Promise;

// listen for requests

app.listen(5001, () => {
    console.log("Server is listening on port 5001");
});

//server connect to db
mongoose.connect(dbConfig.url, {
    useNewUrlParser: true
}).then(() => {
    console.log("Successfully connected to the database");
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});


// define a simple route
app.get('/notes',cors(), (req, res, next) => {
    
});
