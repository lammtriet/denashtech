const MongoClient = require('mongodb').MongoClient;

// replace the uri string with your connection string.
const url = "mongodb+srv://testadmin:testadmin@testtraining-g7nap.mongodb.net/test?retryWrites=true&w=majority"
MongoClient.connect(url, function(err, client) {
   if(err) {
        console.log('Error occurred while connecting to MongoDB Atlas...\n',err);
   }
   console.log('Connected...');


   var collection = client.db("Users").collection("userinfo").find();
   // perform actions on the collection object
    collection.each(function(err,doc){
      console.log(doc);
    });

   client.close();
});
